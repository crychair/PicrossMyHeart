﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Options : MonoBehaviour {

    public AudioMixer mainMixer;
    public GameObject masterVolSlider;
    public GameObject audioSource;
    public bool muted;
    public GameObject mutedToggle;

    public float masterVolume = 0 , musicVolume = 0 , sfxVolume = 0 ;

    void Start()
    {
        LoadOptionsData();
        masterVolSlider.GetComponent<Slider>().value = masterVolume;
        //Debug.Log(muted);
        SetMute(muted);
        SetAllVolume(masterVolume, musicVolume, sfxVolume);
    }

    public void SetMute(bool ismuted)
    {
        
        audioSource.GetComponent<AudioSource>().mute = ismuted;
        SaveMutedData(ismuted);
        mutedToggle.GetComponent<Toggle>().isOn = ismuted;
    }

	public void SetAllVolume (float volume)
    {
        mainMixer.SetFloat("MasterVolume", volume);
        mainMixer.SetFloat("MusicVolume", volume);
        mainMixer.SetFloat("SFXVolume", volume);
        SaveOptionsData(volume, volume, volume);
    }

    public void SetAllVolume(float ppmasterVolume, float ppmusicVolume, float ppsfxVolume)
    {
        mainMixer.SetFloat("MasterVolume", ppmasterVolume);
        mainMixer.SetFloat("MusicVolume", ppmusicVolume);
        mainMixer.SetFloat("SFXVolume", ppsfxVolume);
        SaveOptionsData(ppmasterVolume, ppmusicVolume, ppsfxVolume);
    }

    public void SetMasterVolume(float volume)
    {
        mainMixer.SetFloat("MasterVolume", volume);
        PlayerPrefs.SetFloat("MasterVolume", volume);
    }

    public void SetSFXVolume(float volume)
    {
        mainMixer.SetFloat("SFXVolume", volume);
        PlayerPrefs.SetFloat("SFXVolume", volume);
    }

    public void SetMusicVolume(float volume)
    {
        mainMixer.SetFloat("MusicVolume", volume);
        PlayerPrefs.SetFloat("MusicVolume", volume);
    }


    void SaveOptionsData(float ppmasterVolume, float ppmusicVolume, float ppsfxVolume)
    {
        PlayerPrefs.SetFloat("MasterVolume", ppmasterVolume);
        PlayerPrefs.SetFloat("MusicVolume", ppmusicVolume);
        PlayerPrefs.SetFloat("SFXVolume", ppsfxVolume);
    }

    void SaveMutedData(bool mutedtemp)
    {
        if(mutedtemp == true) {
            PlayerPrefs.SetInt("Muted", 1);
        }
        else {
            PlayerPrefs.SetInt("Muted", 0);
        }
    }

    void LoadOptionsData()
    {
        masterVolume = PlayerPrefs.GetFloat("MasterVolume");
        musicVolume = PlayerPrefs.GetFloat("MusicVolume");
        sfxVolume = PlayerPrefs.GetFloat("SFXVolume");
        if (PlayerPrefs.GetInt("Muted") == 1)
        {
            muted = true; //1 = Muted 0 = Unmuted
        }
        else
        {
            muted = false;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

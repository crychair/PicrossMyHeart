﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public LevelStorage levelStorage;

    //public GameObject grid5x5, grid10x10;

    public GameObject MainMenuCanvas, SettingsMenuCanvas, CreditsCanvas, PuzzleTypeSelectCanvas, FivexFiveLevelSelectCanvas, TenxTenLevelSelectCanvas, GameBoardCanvas;//, GameOverCanvas;

    public enum stateManager
    {
        MainMenu, SettingsMenu, Credits, PuzzleTypeSelect, FivexFiveLevelSelect, TenxTenLevelSelect,  GameBoard,// GameOver,
        //TODO Create NextState Method that will always working with updates to the values here.
    }
    public stateManager gameState = stateManager.MainMenu;
    public List<stateManager> lastState;
    public enum puzzleSize
    {
        FiveXFive = 0, TenXTen = 1, FifteenXFifteen = 2, TwentyXTwenty = 3,
    }
    public puzzleSize puzzleType = puzzleSize.FiveXFive;
    public int levelSelected = 0;
    // Use this for initialization
    void Start () {

        //levelStorage = ScriptableObject.CreateInstance<LevelStorage>();//  new LevelStorage();
        Level temp = GetLevel(1,0);
        Debug.Log(temp.levelImage.name);
        lastState.Add(stateManager.MainMenu);

        //LevelChosen(gameObject.GetComponent<LevelStorage>().FiveXFive_1);
        //GameBoardCanvas.GetComponent<GridCreation>().SetupLevel(gameObject.GetComponent<LevelStorage>().TenXTen_1);
        //parseLevel();
    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(lastState.Count);
	}


    /*void LevelChosen(List<List<int>> level)
    {
        //initially set all grids inactive so two will never be active at once
        grid5x5.SetActive(false);
        grid10x10.SetActive(false);
        //Get Level Size and turn on corresponding grid
        if (level.Count == 5 && level[0].Count == 5) //5x5
        {
            grid5x5.SetActive(true);
            gameObject.GetComponent<GridCreation>().SetupLevel(5, 5, level, grid5x5);
        }
        else if (level.Count == 10 && level[0].Count == 10) //10x10
        {
            grid10x10.SetActive(true);
            gameObject.GetComponent<GridCreation>().SetupLevel(10, 10, level, grid10x10);
        }
        else
        {
            Debug.Log(level.Count);
            Debug.Log(level[0].Count);
            Debug.LogError("Level Size does not match any known grid size");
        }
    }*/

    public void LevelSelect(int level)
    {
        levelSelected = level;
        OpenGameBoardCanvas();
        GameBoardCanvas.GetComponent<GridCreation>().SetupLevel(GetLevel(level, (int)puzzleType));
    }

    public Level GetLevel (int level, int puzzlesize)
    {
        //Level temp = null;
        switch (puzzlesize)
        {
            case 0: // puzzleSize.FiveXFive:
                return levelStorage.FiveXFiveDict[level];
            case 1: // puzzleSize.TenXTen:
                return levelStorage.TenXTenDict[level];
            //case 2:// puzzleSize.FifteenXFifteen:
            //    return levelStorage.FifteenXFifteenDict[level];
            //case 3:// puzzleSize.TwentyXTwenty:
            //    return levelStorage.TwentyXTwentyDict[level];
            default:
                Debug.LogError("No Level to load with type " + puzzlesize + " and level number " + level);
                return null;
        }
        // temp;
    }

    public void PuzzleTypeSelect(int size)
    {
        //FiveXFive = 0, TenXTen = 1, FifteenXFifteen = 2, TwentyXTwenty = 3,
        puzzleType = (puzzleSize)size;
    }


    public void Update(stateManager state)
    {
        UpdateCanvases(state);
    }

    void UpdateCanvases(stateManager state)
    {
        gameState = state;
        bool MainMenuCanvasOn = false;
        bool SettingsMenuCanvasOn = false;
        bool CreditsCanvasOn = false;
        bool PuzzleTypeSelectCanvasOn = false;
        bool FivexFiveLevelSelectCanvasOn = false;
        bool TenxTenLevelSelectCanvasOn = false;
        bool GameBoardCanvasOn = false;
        //bool GameOverCanvasOn = false;

        switch (state)
        {
            case stateManager.MainMenu:
                MainMenuCanvasOn = true;
                break;
            case stateManager.SettingsMenu:
                SettingsMenuCanvasOn = true;
                break;
            case stateManager.Credits:
                CreditsCanvasOn = true;
                break;
            case stateManager.PuzzleTypeSelect:
                PuzzleTypeSelectCanvasOn = true;
                break;
            case stateManager.FivexFiveLevelSelect:
                FivexFiveLevelSelectCanvasOn = true;
                break;
            case stateManager.TenxTenLevelSelect:
                TenxTenLevelSelectCanvasOn = true;
                break;
            case stateManager.GameBoard:
                GameBoardCanvasOn = true;
                break;
            //case stateManager.GameOver:
            //    GameOverCanvasOn = true;
             //   break;
            default:
                break;
        }

        MainMenuCanvas.SetActive(MainMenuCanvasOn);
        SettingsMenuCanvas.SetActive(SettingsMenuCanvasOn);
        CreditsCanvas.SetActive(CreditsCanvasOn);
        PuzzleTypeSelectCanvas.SetActive(PuzzleTypeSelectCanvasOn);
        FivexFiveLevelSelectCanvas.SetActive(FivexFiveLevelSelectCanvasOn);
        TenxTenLevelSelectCanvas.SetActive(TenxTenLevelSelectCanvasOn);
        GameBoardCanvas.SetActive(GameBoardCanvasOn);
        //GameOverCanvas.SetActive(GameOverCanvasOn);

    }

    public void OpenMainMenuCanvas()
    {
        lastState.Add(gameState);
        UpdateCanvases(stateManager.MainMenu);
    }

    public void OpenSettingsMenuCanvas()
    {
        lastState.Add(gameState);
        UpdateCanvases(stateManager.SettingsMenu);
    }

    public void OpenCreditsCanvas()
    {
        lastState.Add(gameState);
        UpdateCanvases(stateManager.Credits);
    }

    public void OpenPuzzleTypeSelectCanvas()
    {
        lastState.Add(gameState);
        UpdateCanvases(stateManager.PuzzleTypeSelect);
    }

    public void OpenFivexFiveLevelSelectCanvas()
    {
        lastState.Add(gameState);
        UpdateCanvases(stateManager.FivexFiveLevelSelect);
    }
    public void OpenTenxTenLevelSelectCanvas()
    {
        lastState.Add(gameState);
        UpdateCanvases(stateManager.TenxTenLevelSelect);
    }

    public void OpenGameBoardCanvas()
    {
        lastState.Add(gameState);
        UpdateCanvases(stateManager.GameBoard);
    }

    //public void OpenGameOverCanvas()
    //{
    //    lastState.Add(gameState);
    //    UpdateCanvases(stateManager.GameOver);
    //}

    public void OpenNextLevel()
    {
        LevelSelect(levelSelected + 1);
    }
    public void OpenLastCanvas()
    {
        stateManager temp = lastState[lastState.Count - 1];
        lastState.RemoveAt(lastState.Count - 1);
        UpdateCanvases(temp);
    }






    //PLAYER PREFS
    public void LoadPlayerData() {


    }

    public void SavePlayerData() {

    }

    public void SaveLevelData(string levelName, float time)
    {
        PlayerPrefs.SetFloat(levelName, time);
    }

    public bool HasKey(string playerprefscheck)
    {
        return PlayerPrefs.HasKey(playerprefscheck);
    }

    public float GetLevelTime(string levelName)
    {
        return PlayerPrefs.GetFloat(levelName);
    }
}


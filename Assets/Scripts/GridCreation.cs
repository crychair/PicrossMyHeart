﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using CnControls;

public class GridCreation : MonoBehaviour
{
    public int numberOfColumns = 5;
    public int numberOfRows = 5;
    public List<List<GameObject>> grid;
    public List<List<int>> levelLayout;
    public GameObject girdBounds;
    public List<GameObject> horizontalTextList;
    public List<GameObject> verticalTextList;
    public GameObject gridItemPrefab, horizontalText, verticalText;
    public GameObject horizontal5x5Text, vertical5x5Text;

    public GameObject horizontal10x10Text, vertical10x10Text;
    public GameObject horizontal15x15Text, vertical15x15Text;
    public GameObject horizontal20x20Text, vertical20x20Text;

    //Vert/Horizontal Text Profiles
    public int verticalTextYShift = 200;//, verticalTextXShift = 10;
    public int horizontalTextXShift = -230;//, horizontalTextYShift = 10;
    //public float scale

    //public string[] rows, columns;
    public string[] rowNumList, colNumList;
    public GameObject manager;
    //public GameObject currentSelection, previousSelection = null;
    public int currentPosX = 1, currentPosY = 1;
    public string levelName;
    public GameObject gameTimerText;
    public bool gameTimerEnabled = false;
    public float gameTimer = 0f;

    //Game Over Things
    public GameObject gameOverCanvas;
    public GameObject levelTitle;
    Sprite levelImage;
    public GameObject levelImageDisplay;

    //Grid Item Animations
    //public RuntimeAnimatorController toggleController, HalfAnimsController, PressController;

    public GameObject toggleButton;

    //GameOver
    public bool gameover = false;
    public Material gridMat;
    public Color gridFadeColorStart;
    public Color gridFadeColorEnd;
    public float gridFadeTime = 1.0f;
    public Material gameOverMat;
    public Color gameOverFadeColorStart;
    public Color gameOverFadeColorEnd;


    /*public void SetupLevel(int rows, int cols, List<List<int>> level, GameObject activegrid)
    {
        numberOfRows = rows;
        numberOfColumns = cols;
        activeGrid = activegrid;
        grid = activeGrid.GetComponent<GridStruct>().grid;
        currentSelection = grid[currentPosX][currentPosY];
        previousSelection = grid[currentPosX][currentPosY];
        levelLayout = level;
        SetupLevel(level);

    }*/

    void OnEnable() {

        gameTimer = 0f;
        gameover = false;
        gridMat.color = gridFadeColorStart;
        gameOverMat.color = gameOverFadeColorStart;
    }

    // Use this for initialization
    //public void SetupLevel(string[,] level) {
    public void SetupLevel(Level level)
    {
        //Debug.Log(level.GetPuzzleSize());
        if(level.GetPuzzleSize() == "5x5") {
            horizontalText = horizontal5x5Text;
            verticalText = vertical5x5Text;
            verticalTextYShift = 250;
            //verticalTextXShift = 10;
            //horizontalTextYShift = 10;
            horizontalTextXShift = -230;
        }
        else if (level.GetPuzzleSize() == "10x10")
        {
            horizontalText = horizontal10x10Text;
            verticalText = vertical10x10Text;
            verticalTextYShift = 170;
            //verticalTextXShift = 10;
            //horizontalTextYShift = 10;
            horizontalTextXShift = -150;
        }
        /*else if (level.GetPuzzleSize() == "15x15")
        {
            horizontalText = horizontal15x15Text;
            verticalText = vertical15x15Text;
            verticalTextYShift = 130;
            //verticalTextXShift = 10;
            //horizontalTextYShift = 10;
            horizontalTextXShift = -130;
        }
        else if (level.GetPuzzleSize() == "20x20")
        {
            horizontalText = horizontal20x20Text;
            verticalText = vertical20x20Text;
            verticalTextYShift = 110;
            //verticalTextXShift = 10;
            //horizontalTextYShift = 10;
            horizontalTextXShift = -110;
        }*/



        //gameOverCanvas.SetActive(false);
        numberOfColumns = level.levelSizeColumns;
        numberOfRows = level.levelSizeRows;
        parseLevel(level.levelLayout);
        levelLayout = level.levelLayout;
        levelName = level.levelName;
        levelImageDisplay.GetComponent<Image>().sprite = level.levelImage;
        if (levelImageDisplay.GetComponent<Image>().sprite == null)
        {
            Debug.Log("Level Pciture Not Loaded for " + levelName + ".");
        }
        levelTitle.GetComponent<Text>().text = levelName;
        GridSetup();
        gameTimerEnabled = true;
        //createGrid();
        //Debug.Log(grid[1][1].name);
        //UpdateSelected();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (CnInputManager.GetButtonDown("Change"))
        {
            currentSelection.GetComponentInChildren<GridObjectState>().NextState();
        }

        if (CnInputManager.GetButtonDown("Left"))
        {
            MoveLeft();
        }

        if (CnInputManager.GetButtonDown("Right"))
        {
            MoveRight();
        }

        if (CnInputManager.GetButtonDown("Up"))
        {
            MoveUp();
        }
        if (CnInputManager.GetButtonDown("Down"))
        {
            MoveDown();
        }*/
        if (gameTimerEnabled)
        {
            UpdateGameTimer();
            CompareGrid();
        }

        if(gameover) {
            GridFadeOut();
            GameOverFadeIn();
            //PuzzlePicFadeIn();
        }
    }

    public void DeleteGrid()
    {
        for (int i = 0; i < grid.Count; i++) //Rows
        {
            for (int j = 0; j < grid[i].Count; j++)
            {
                Destroy(grid[i][j]);
            }
        }

        for (int i = 0; i < horizontalTextList.Count; i++)
        {
            Destroy(horizontalTextList[i]);
        }

        for (int i = 0; i < verticalTextList.Count; i++)
        {
            Destroy(verticalTextList[i]);
        }


    }

    void CompareGrid()
    {
        bool correct = true;
        if (grid != null)
        {
            for (int i = 0; i < grid.Count; i++) //Rows
            {
                //Debug.Log(i);
                for (int j = 0; j < grid[i].Count; j++)
                {
                    //Debug.Log(i + ":i " + j + ":j " + grid[i][j].gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Filled"));
                    //Debug.Log(i + ":i " + j + ":j " + levelLayout[i][j]);
                    if (levelLayout[i][j] == 1 && !grid[i][j].gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Filled"))
                    {
                        correct = false; //Should be filled and isn't
                    }
                    else if (levelLayout[i][j] == 0 && grid[i][j].gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Filled"))
                    {
                        correct = false; //Shouldn't be filled and is
                    }
                    //Debug.Log(i + " " + j + " " + grid[i][j].GetComponentInChildren<GridObjectState>().isFilled() + " " + levelLayout[i][j] + " " + correct);
                }
            }

            //Debug.Log(correct);
            if (correct)
            {
                gameTimerEnabled = false;
                if (manager.GetComponent<GameManager>().HasKey(levelName)) {
                    if (manager.GetComponent<GameManager>().GetLevelTime(levelName) > gameTimer) {
                        manager.GetComponent<GameManager>().SaveLevelData(levelName, gameTimer);
                    }
                } else {
                    manager.GetComponent<GameManager>().SaveLevelData(levelName, gameTimer);
                }
                manager.GetComponent<GameManager>().SaveLevelData(levelName, gameTimer);

                gameTimer = 0f;
                //WINNING
                gameover = true;
                //TODO START GAME OVER
                //manager.GetComponent<GameManager>().OpenGameOverCanvas();
            }
        }
    }

    void FindHints()
    {
        for (int i = 0; i < grid.Count; i++) //Rows
        {
            //Debug.Log(i);
            for (int j = 0; j < grid[i].Count; j++)
            {

            }
        }
    }

    void UpdateGameTimer()
    {
        gameTimer += Time.deltaTime;
        //string tempMin = ((int)gameTimer / 60).ToString("00");
        //string tempSec = (gameTimer % 60).ToString("f2");
        //gameTimerText.GetComponent<TextMeshProUGUI>().text = tempMin + ":" + tempSec;
        int tempMin = (int)gameTimer / 60;
        int tempSec = (int)gameTimer % 60;
        int tempMili = (int)((gameTimer * 100) % 100);
        string temp =  string.Format("{0:00}:{1:00}.{2:00}", tempMin, tempSec, tempMili);
        gameTimerText.GetComponent<Text>().text = temp;
    }

    void parseLevel(List<List<int>> level)
    {
        rowNumList = new string[level.Count]; //Bottom to Top
        colNumList = new string[level[0].Count];//Left to Right

        for (int i = 0; i < level.Count; i++) //Rows
        {
            string rownumbers = "";
            int previous = 0;
            bool isZero = true;
            int rowlength = level[0].Count;
            for (int j = 0; j < level[0].Count; j++)
            {
                //Debug.Log(i + " " + j + " " + level[i][j]);
                if (level[i][j] == 1)
                {
                    previous += 1;
                }
                if (level[i][j] == 0 || (j == level[0].Count - 1))
                {
                    if (previous > 0)
                    {
                        rownumbers = string.Concat(rownumbers, previous + " ");
                        isZero = false;
                        previous = 0;
                    }
                    else if (isZero && (j == level.Count - 1))
                    {
                        rownumbers = string.Concat(rownumbers, "0");
                    }
                }
            }
            //Debug.Log(rownumbers);
            rowNumList[i] = rownumbers;
            //activeGrid.GetComponent<GridStruct>().rowLabels[i].GetComponent<Text>().text = rownumbers;
        }

        for (int i = 0; i < level.Count; i++) //cols
        {
            string colnumbers = "";
            int previous = 0;
            bool isZero = true;
            int rowlength = level[0].Count;
            for (int j = 0; j < level[0].Count; j++)
            {
                if (level[j][i] == 1)
                {
                    previous += 1;
                }
                if (level[j][i] == 0 || (j == level[0].Count - 1))
                {
                    if (previous > 0)
                    {
                        colnumbers = string.Concat(colnumbers, previous + "\n");
                        isZero = false;
                        previous = 0;
                    }
                    else if (isZero && (j == level.Count - 1))
                    {
                        colnumbers = string.Concat(colnumbers, "0");
                    }
                }
            }
            colNumList[i] = colnumbers;
            //Debug.Log(colnumbers);
            //activeGrid.GetComponent<GridStruct>().colLabels[i].GetComponent<Text>().text = colnumbers;
        }
    }

    /*void UpdateSelected()
    {
        previousSelection.GetComponentInChildren<GridObjectState>().selected = false;
        currentSelection.GetComponentInChildren<GridObjectState>().selected = true;
    }*/

    /*public void MoveLeft()
    {
        Move(-1, 0);
    }

    public void MoveRight()
    {
        Move(1, 0);
    }

    public void MoveUp()
    {
        Move(0, -1);
    }

    public void MoveDown()
    {
        Move(0, 1);
    }

    public void Move(int x, int y)
    {
        previousSelection = currentSelection;
        //TODO: check out of bounds
        currentPosY += y;
        currentPosX += x;
        currentSelection = grid[currentPosY][currentPosX];
        //TODO Grid is y then X. That is stupid
        UpdateSelected();
    }*/

    private void GridSetup()
    {
        grid = new List<List<GameObject>>();
        float xbounds = girdBounds.GetComponent<RectTransform>().rect.width;
        float ybounds = girdBounds.GetComponent<RectTransform>().rect.height;
        float xStartPosition = girdBounds.GetComponent<RectTransform>().localPosition.x - xbounds / 2;
        float yStartPosition = girdBounds.GetComponent<RectTransform>().localPosition.y + ybounds / 2;
        float xSizeOfGirdItems = xbounds / numberOfColumns;
        float ySizeOfGirdItems = ybounds / numberOfRows;

        for (int i = 0; i < numberOfRows; i++) //Left to Right, Top to Bottom
        {
            List<GameObject> gridRow = new List<GameObject>();
            for (int j = 0; j < numberOfColumns; j++)
            {
                GameObject griditem = Instantiate(gridItemPrefab, new Vector3 ( 0,0,0), Quaternion.identity);
                griditem.transform.SetParent(this.transform, false);
                float xPos = xStartPosition + (xSizeOfGirdItems / 2 + xSizeOfGirdItems * j);
                float yPos = yStartPosition + (-ySizeOfGirdItems / 2 - ySizeOfGirdItems * i);
                griditem.GetComponent<RectTransform>().sizeDelta = new Vector2(xSizeOfGirdItems, ySizeOfGirdItems);
                griditem.GetComponent<RectTransform>().localPosition = new Vector3(xPos, yPos, 0);
                griditem.name = j + " " + i;
                griditem.GetComponent<GridItemAnimation>().toggleButton = toggleButton;
                griditem.GetComponent<GridItemAnimation>().manager = manager;
                gridRow.Add(griditem);
                //Debug.Log("xStart " + xStartPosition);
                //Debug.Log("yStart " + yStartPosition);
                //Debug.Log("xSize " + xSizeOfGirdItems);
                //Debug.Log("ySize " + ySizeOfGirdItems);
                //Debug.Log("j " + j);
                //Debug.Log("i " + i);
            }
            grid.Add(gridRow);
        }

        for (int i = 0; i < numberOfColumns; i++) //Vertical Text Left to Right
        {
            GameObject verticalTextItem = Instantiate(verticalText, new Vector3(0, 0, 0), Quaternion.identity);
            //verticalTextItem.transform.localScale = new Vector3(textScale, textScale, 0);
            verticalTextItem.transform.SetParent(this.transform, false);
            //verticalTextItem.GetComponent<RectTransform>().localScale = new Vector3(textScale, textScale, 0);
            float xPos = xStartPosition + (xSizeOfGirdItems / 2 + xSizeOfGirdItems * i);
            float yPos = yStartPosition + verticalTextYShift;
            verticalTextItem.GetComponent<RectTransform>().localPosition = new Vector3(xPos, yPos, 0);
            verticalTextItem.GetComponent<RectTransform>().sizeDelta = new Vector2(xSizeOfGirdItems, verticalTextItem.GetComponent<RectTransform>().sizeDelta.y);
            //verticalTextItem.GetComponent<RectTransform>(). = new Vector3(textScale, textScale, 0);
            verticalTextItem.GetComponent<Text>().text = colNumList[i];
            verticalTextList.Add(verticalTextItem);
        }

        for (int i = 0; i < numberOfRows; i++) //Vertical Text Left to Right
        {
            GameObject horizontalTextItem = Instantiate(horizontalText, new Vector3(0, 0, 0), Quaternion.identity);
            horizontalTextItem.transform.SetParent(this.transform, false);
            //horizontalTextItem.GetComponent<RectTransform>().localScale = new Vector3(textScale, textScale, 0);
            float xPos = xStartPosition + horizontalTextXShift;
            float yPos = yStartPosition + (ySizeOfGirdItems / 2 - ySizeOfGirdItems * (i + 1));
            horizontalTextItem.GetComponent<RectTransform>().localPosition = new Vector3(xPos, yPos, 0);
            horizontalTextItem.GetComponent<RectTransform>().sizeDelta = new Vector2(horizontalTextItem.GetComponent<RectTransform>().sizeDelta.x, ySizeOfGirdItems);
            //horizontalTextItem.GetComponent<RectTransform>().localScale = new Vector3(textScale, textScale, 0);
            horizontalTextItem.GetComponent<Text>().text = rowNumList[i];
            horizontalTextList.Add(horizontalTextItem);
        }


    }

    /*public void ChangeButton()
    {
        currentSelection.GetComponent<SquareState>().NextState();
        currentSelection.GetComponent<SquareState>().ChangeDisplay();
    }*/

    void GridFadeOut()
    {
        gridMat.color = Color.Lerp(gridMat.color, gridFadeColorEnd, gridFadeTime * Time.deltaTime);
    }

    void GameOverFadeIn()
    {
        gameOverMat.color = Color.Lerp(gameOverMat.color, gameOverFadeColorEnd, gridFadeTime * Time.deltaTime);
    }

}

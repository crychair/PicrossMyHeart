﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSprites", menuName = "Levels/sprtieList")]
public class LevelStorage : ScriptableObject {

    //When creating new levels this is the layout they will be seen on the screen.
    //It is slightly confusing to ierate though, because there is no easy way to get each individual column.
    //Instead each List with thin the main level List is a new row. And we verify that all rows have the
    //correct ammount of characters and that there are the same amount of total rows. The amount of rows
    //in the puzzle is the same as the length of the columns.
    //5x5 Levels
    public Dictionary<int, Level> FiveXFiveDict;
    public Sprite FiveXFive_1_Sprite;
    public Sprite FiveXFive_2_Sprite;
    public Sprite FiveXFive_3_Sprite;
    public Sprite FiveXFive_4_Sprite;
    public Sprite FiveXFive_5_Sprite;
    public Sprite FiveXFive_6_Sprite;
    public Sprite FiveXFive_7_Sprite;
    public Sprite FiveXFive_8_Sprite;
    public Sprite FiveXFive_9_Sprite;
    public Sprite FiveXFive_10_Sprite;
    public Sprite FiveXFive_11_Sprite;
    public Sprite FiveXFive_12_Sprite;
    public Sprite FiveXFive_13_Sprite;
    public Sprite FiveXFive_14_Sprite;
    public Sprite FiveXFive_15_Sprite;
    /// 10x10 Levels
    public Dictionary<int, Level> TenXTenDict;
    public Sprite TenXTen_1_Sprite;
    public Sprite TenXTen_2_Sprite;
    public Sprite TenXTen_3_Sprite;
    public Sprite TenXTen_4_Sprite;
    public Sprite TenXTen_5_Sprite;
    public Sprite TenXTen_6_Sprite;
    public Sprite TenXTen_7_Sprite;
    public Sprite TenXTen_8_Sprite;
    public Sprite TenXTen_9_Sprite;
    public Sprite TenXTen_10_Sprite;
    public Sprite TenXTen_11_Sprite;
    public Sprite TenXTen_12_Sprite;
    public Sprite TenXTen_13_Sprite;
    public Sprite TenXTen_14_Sprite;
    public Sprite TenXTen_15_Sprite;

    /// 15x15 Levels
    //public Dictionary<int, Level> FifteenXFifteenDict;
    //public Sprite FifteenXFifteen_1_Sprite;
    //public Sprite FifteenXFifteen_2_Sprite;
    //public Sprite FifteenXFifteen_3_Sprite;
    //public Sprite FifteenXFifteen_4_Sprite;

    /// 20x20 Levels
    //public Dictionary<int, Level> TwentyXTwentyDict;
    //public Sprite TwentyXTwenty_1_Sprite;
    //public Sprite TwentyXTwenty_2_Sprite;
    //public Sprite TwentyXTwenty_3_Sprite;
    //public Sprite TwentyXTwenty__Sprite;


    void OnEnable()
    {    
        Level FiveXFive_1 = new Level(5, 5, "Heart", FiveXFive_1_Sprite,
                                new List<List<int>>() { new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 0, 0, 1, 0, 0 }});
        Level FiveXFive_2 = new Level(5, 5, "Sword", FiveXFive_2_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 1, 1 },
                                                        new List<int> { 1, 0, 1, 1, 1 },
                                                        new List<int> { 1, 1, 1, 1, 0 },
                                                        new List<int> { 0, 1, 1, 0, 0 },
                                                        new List<int> { 1, 0, 1, 1, 0 }});
        Level FiveXFive_3 = new Level(5, 5, "Rook", FiveXFive_3_Sprite,
                                new List<List<int>>() { new List<int> { 1, 0, 1, 0, 1 },
                                                        new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 1, 1, 1, 1, 1 }});
        Level FiveXFive_4 = new Level(5, 5, "Flower", FiveXFive_4_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 1, 0, 0 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 0, 0, 1, 0, 0 },
                                                        new List<int> { 1, 0, 1, 0, 1 },
                                                        new List<int> { 0, 1, 1, 1, 0 }});
        Level FiveXFive_5 = new Level(5, 5, "Anchor", FiveXFive_5_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 1, 0, 0 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 0, 0, 1, 0, 0 },
                                                        new List<int> { 1, 0, 1, 0, 1 },
                                                        new List<int> { 0, 1, 1, 1, 0 }});
        Level FiveXFive_6 = new Level(5, 5, "Box", FiveXFive_6_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 0 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 0, 0, 0, 0, 0 }});
        Level FiveXFive_7 = new Level(5, 5, "Bird", FiveXFive_7_Sprite,
                                new List<List<int>>() { new List<int> { 1, 1, 0, 1, 1 },
                                                        new List<int> { 1, 1, 0, 1, 1 },
                                                        new List<int> { 0, 1, 1, 0, 0 },
                                                        new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 1, 1, 0, 1, 1 }});
        Level FiveXFive_8 = new Level(5, 5, "Small Bow", FiveXFive_8_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 1, 1, 0 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 1, 0, 0, 1, 0 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 0, 0, 1, 1, 0 }});
        Level FiveXFive_9 = new Level(5, 5, "Bow Tie", FiveXFive_9_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 0 },
                                                        new List<int> { 1, 1, 0, 1, 1 },
                                                        new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 1, 1, 0, 1, 1 },
                                                        new List<int> { 0, 0, 0, 0, 0 }});
        Level FiveXFive_10 = new Level(5, 5, "Skull", FiveXFive_10_Sprite,
                                new List<List<int>>() { new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 1, 0, 1, 0, 1 },
                                                        new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 0, 0, 0, 0, 0 }});
        Level FiveXFive_11 = new Level(5, 5, "Pi", FiveXFive_11_Sprite,
                                new List<List<int>>() { new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 1, 1, 0, 1, 0 }});
        Level FiveXFive_12 = new Level(5, 5, "Smile", FiveXFive_12_Sprite,
                                new List<List<int>>() { new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 0, 0, 0, 0, 0 },
                                                        new List<int> { 1, 0, 0, 0, 1 },
                                                        new List<int> { 0, 1, 1, 1, 0 }});
        Level FiveXFive_13 = new Level(5, 5, "Man", FiveXFive_13_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 1, 0, 0 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 1, 0, 1, 0, 1 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 0, 1, 0, 1, 0 }});
        Level FiveXFive_14 = new Level(5, 5, "Hashtag", FiveXFive_14_Sprite,
                                new List<List<int>>() { new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 0, 1, 0, 1, 0 },
                                                        new List<int> { 1, 1, 1, 1, 1 },
                                                        new List<int> { 0, 1, 0, 1, 0 }});
        Level FiveXFive_15 = new Level(5, 5, "Grave", FiveXFive_15_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 1, 0, 0 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 0, 0, 1, 0, 0 },
                                                        new List<int> { 0, 1, 1, 1, 0 },
                                                        new List<int> { 1, 1, 1, 1, 1 }});

        FiveXFiveDict = new Dictionary<int, Level>();
        FiveXFiveDict.Add(1, FiveXFive_1);
        FiveXFiveDict.Add(2, FiveXFive_2);
        FiveXFiveDict.Add(3, FiveXFive_3);
        FiveXFiveDict.Add(4, FiveXFive_4);
        FiveXFiveDict.Add(5, FiveXFive_5);
        FiveXFiveDict.Add(6, FiveXFive_6);
        FiveXFiveDict.Add(7, FiveXFive_7);
        FiveXFiveDict.Add(8, FiveXFive_8);
        FiveXFiveDict.Add(9, FiveXFive_9);
        FiveXFiveDict.Add(10, FiveXFive_10);
        FiveXFiveDict.Add(11, FiveXFive_11);
        FiveXFiveDict.Add(12, FiveXFive_12);
        FiveXFiveDict.Add(13, FiveXFive_13);
        FiveXFiveDict.Add(14, FiveXFive_14);
        FiveXFiveDict.Add(15, FiveXFive_15);

        Level TenXTen_1 = new Level(10, 10, "Heart", TenXTen_1_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}});
    /*new List<List<int>>() {     new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}});*/
        Level TenXTen_2 = new Level(10, 10, "Long Bow", TenXTen_2_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 0, 0, 0, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 0, 0, 0, 0, 1, 1, 0},
                                                        new List<int> { 1, 1, 0, 0, 0, 0, 0, 0, 1, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 0, 0, 0, 0, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 1, 0, 0, 0, 1, 1, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 1, 1, 0, 0}});
        Level TenXTen_3 = new Level(10, 10, "Arrows", TenXTen_3_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 1, 0, 0, 1, 1, 0, 1, 1},
                                                        new List<int> { 0, 1, 1, 1, 0, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 0, 1, 1, 1, 0},
                                                        new List<int> { 1, 1, 0, 1, 1, 0, 0, 1, 0, 0}});
        Level TenXTen_4 = new Level(10, 10, "Tree", TenXTen_4_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}});
        Level TenXTen_5 = new Level(10, 10, "Champange", TenXTen_5_Sprite,
                                new List<List<int>>() { new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0}});
        Level TenXTen_6 = new Level(10, 10, "Umbrella", TenXTen_6_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 0, 1, 0, 0, 1, 1},
                                                        new List<int> { 1, 1, 1, 0, 0, 1, 0, 0, 0, 1},
                                                        new List<int> { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 1, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 1, 1, 1, 0, 0}});
        Level TenXTen_7 = new Level(10, 10, "Wine", TenXTen_7_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
                                                        new List<int> { 0, 1, 0, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 0, 0, 1, 1, 1, 0}});
        Level TenXTen_8 = new Level(10, 10, "Pacman", TenXTen_8_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 0, 0, 0, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 0, 0, 0, 0, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 0, 1, 0, 0, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}});
        Level TenXTen_9 = new Level(10, 10, "Gameboy", TenXTen_9_Sprite,
                                new List<List<int>>() { new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 0, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 1, 0, 1, 1, 0},
                                                        new List<int> { 0, 1, 1, 0, 1, 1, 1, 0, 1, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0}});
        Level TenXTen_10 = new Level(10, 10, "Vial", TenXTen_10_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0}});
        Level TenXTen_11 = new Level(10, 10, "Casette", TenXTen_11_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 0, 1, 0, 1, 0, 1, 0, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                                                        new List<int> { 1, 0, 1, 1, 0, 0, 1, 1, 0, 1},
                                                        new List<int> { 1, 0, 1, 1, 0, 0, 1, 1, 0, 1},
                                                        new List<int> { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                                                        new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}});
        Level TenXTen_12 = new Level(10, 10, "Clam", TenXTen_12_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 1, 1, 0, 1, 1, 0, 1, 1, 0},
                                                        new List<int> { 1, 0, 1, 0, 1, 1, 0, 1, 0, 1},
                                                        new List<int> { 1, 0, 1, 0, 1, 1, 0, 1, 0, 1},
                                                        new List<int> { 1, 0, 1, 0, 1, 1, 0, 1, 0, 1},
                                                        new List<int> { 1, 0, 1, 0, 1, 1, 0, 1, 0, 1},
                                                        new List<int> { 0, 1, 0, 1, 0, 0, 1, 0, 1, 0},
                                                        new List<int> { 0, 0, 1, 0, 1, 1, 0, 1, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0}});
        Level TenXTen_13 = new Level(10, 10, "Clock", TenXTen_13_Sprite,
                                new List<List<int>>() { new List<int> { 0, 1, 1, 0, 0, 0, 0, 1, 1, 0},
                                                        new List<int> { 1, 1, 0, 0, 0, 0, 0, 0, 1, 1},
                                                        new List<int> { 1, 0, 1, 1, 1, 1, 1, 1, 0, 1},
                                                        new List<int> { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 1, 0, 0, 1, 0},
                                                        new List<int> { 0, 1, 0, 1, 1, 0, 0, 0, 1, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 0, 1, 0, 0}});
        Level TenXTen_14 = new Level(10, 10, "Upvote", TenXTen_14_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}});
        Level TenXTen_15 = new Level(10, 10, "Downvote", TenXTen_15_Sprite,
                                new List<List<int>>() { new List<int> { 1, 1, 1, 1, 0, 0, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 0, 0, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 0, 0, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 0, 0, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 0, 0, 1, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 0, 0, 1, 1, 1, 1},
                                                        new List<int> { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                                                        new List<int> { 1, 1, 0, 0, 0, 0, 0, 0, 1, 1},
                                                        new List<int> { 1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
                                                        new List<int> { 1, 1, 1, 1, 0, 0, 1, 1, 1, 1}});

        TenXTenDict = new Dictionary<int, Level>();
        TenXTenDict.Add(1, TenXTen_1);
        TenXTenDict.Add(2, TenXTen_2);
        TenXTenDict.Add(3, TenXTen_3);
        TenXTenDict.Add(4, TenXTen_4);
        TenXTenDict.Add(5, TenXTen_5);
        TenXTenDict.Add(6, TenXTen_6);
        TenXTenDict.Add(7, TenXTen_7);
        TenXTenDict.Add(8, TenXTen_8);
        TenXTenDict.Add(9, TenXTen_9);
        TenXTenDict.Add(10, TenXTen_10);
        TenXTenDict.Add(11, TenXTen_11);
        TenXTenDict.Add(12, TenXTen_12);
        TenXTenDict.Add(13, TenXTen_13);
        TenXTenDict.Add(14, TenXTen_14);
        TenXTenDict.Add(15, TenXTen_15);

        /*Level FifteenXFifteen_1 = new Level(15, 15, "Rose2", FifteenXFifteen_1_Sprite,
                                new List<List<int>>() { new List<int> { 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1},
                                                        new List<int> { 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1},
                                                        new List<int> { 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1},
                                                        new List<int> { 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1},
                                                        new List<int> { 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1},
                                                        new List<int> { 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1},
                                                        new List<int> { 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1},
                                                        new List<int> { 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1},
                                                        new List<int> { 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0},
                                                        new List<int> { 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1},
                                                        new List<int> { 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1}});

        Level FifteenXFifteen_2 = new Level(15, 15, "Rose", FifteenXFifteen_2_Sprite,
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0},
                                                        new List<int> { 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0}});

        FifteenXFifteenDict = new Dictionary<int, Level>();
        FifteenXFifteenDict.Add(1, FifteenXFifteen_1);
        FifteenXFifteenDict.Add(2, FifteenXFifteen_2);

        Level TwentyXTwenty_1 = new Level(20, 20, "Rose2", //(Sprite)AssetDatabase.LoadAssetAtPath("Assets/Pyxel/Levels/20x20/Rose.png", typeof(Sprite)),
                                new List<List<int>>() { new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                                                        new List<int> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0}});

        TwentyXTwentyDict = new Dictionary<int, Level>();
        TwentyXTwentyDict.Add(1, TwentyXTwenty_1);*/
    }

}


public class Level {
    public List<List<int>> levelLayout;
    public Sprite levelImage;
    public int levelSizeRows, levelSizeColumns;
    public string levelName;

    public Level(int levelSizeRows, int levelSizeColumns, string levelName, Sprite levelImage, List<List<int>> levelLayout) {
        this.levelLayout = levelLayout;
        this.levelSizeRows = levelSizeRows;
        this.levelSizeColumns = levelSizeColumns;
        this.levelName = levelName;
        this.levelImage = levelImage;

        CheckLevel();
    }

    public Level(int levelSizeRows, int levelSizeColumns, string levelName, List<List<int>> levelLayout)
    {
        this.levelLayout = levelLayout;
        this.levelSizeRows = levelSizeRows;
        this.levelSizeColumns = levelSizeColumns;
        this.levelName = levelName;

        CheckLevel();
    }

    public void CheckLevel() {
        if(levelLayout.Count != levelSizeColumns) {
            Debug.Log(levelName + " does not have the correct column size.");
        } else {
            int rowCount = 0;
            foreach (var row in levelLayout)
            {
                if(row.Count != levelSizeRows) {
                    Debug.Log(levelName + " does not have the correct row size on row " + rowCount);
                }
                rowCount++;
            }
        }
    }

    public string GetPuzzleSize()
    {
        return levelSizeRows + "x" + levelSizeColumns;
    }


}

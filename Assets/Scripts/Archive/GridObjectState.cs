﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridObjectState : MonoBehaviour {

    private GameObject Empty, Filled, Crossed, Marked;
    //public GameObject Selected;
    public Color selectedColor = Color.grey;

    void Start()
    {
        if(gameObject == null) {
            Debug.Log("fuck me");
        }
        if (gameObject.GetComponent<Button>() == null)
        {
            Debug.Log("fuck me");
            Debug.Log(gameObject.name);
            Debug.Log(gameObject.transform.parent.name);
        }
        gameObject.GetComponent<Button>().onClick.AddListener(Click);
        //Filled = transform.Find("Filled").gameObject;
        Crossed = transform.Find("Crossed").gameObject;
        Marked = transform.Find("Marked").gameObject;
    }

    public enum State
    {
        Empty,
        Filled,
        Crossed,
        Marked,
    }

    public bool selected = false;

    public State state;

    public void NextState()
    {
        if (state == State.Empty)
        {
            state = State.Filled;
        }
        else if (state == State.Filled)
        {
            state = State.Crossed;
        }
        else if (state == State.Crossed)
        {
            state = State.Marked;
        }
        else if (state == State.Marked)
        {
            state = State.Empty;
        }

        ChangeDisplay();
    }

    public void ChangeDisplay()
    {
        if (state == State.Crossed)
        {
            Crossed.SetActive(true);
            Filled.SetActive(false);
            Marked.SetActive(false);
        }
        else if (state == State.Filled)
        {
            Crossed.SetActive(false);
            Filled.SetActive(true);
            Marked.SetActive(false);
        }
        else if (state == State.Marked)
        {
            Crossed.SetActive(false);
            Filled.SetActive(false);
            Marked.SetActive(true);
        }
        else if (state == State.Empty)
        {
            Crossed.SetActive(false);
            Filled.SetActive(false);
            Marked.SetActive(false);
        }
    }

    public void Click()
    {
        //if(selected) {
        Debug.Log("Clicked");
        NextState();
        //}
    }

    void OnMouseDown()
    {
        Click();
    }

    void OnMouseOver()
    {
        selected = true;
    }

    void OnMouseExit()
    {
        selected = false;
    }

    void Update()
    {
        print(gameObject.GetComponent<Animator>().name);
        if(gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Filled"))
        {
            state = State.Filled;
            print("Filled");
        } 
        else if (gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Crossed"))
        {
            state = State.Crossed;
            print("Crossed");
        } 
        else if (gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Marked"))
        {
            state = State.Marked;
            print("Marked");
        }
        else 
        {
            state = State.Empty;
        }

        if (selected == true)
        {
            gameObject.GetComponent<Image>().color = selectedColor;
            //Selected.SetActive(true);
        }
        else
        {
            gameObject.GetComponent<Image>().color = Color.white;
            //Selected.SetActive(false);
        }


    }

    public bool isCrossed()
    {
        if (state == State.Crossed)
        {
            return true;
        } else {
            return false;
        }
    }

    public bool isFilled()
    {
        if (state == State.Filled)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

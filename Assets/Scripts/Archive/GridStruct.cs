﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridStruct : MonoBehaviour {
    public GameObject gridItemPrefab;
    public List<List<GameObject>> grid = new List<List<GameObject>>();
    public List<GameObject> rowLabels = new List<GameObject>();
    public List<GameObject> colLabels = new List<GameObject>();
    public bool isloading = true;
    void Awake () {
        int count = 0;
        foreach (Transform child in gameObject.transform)
        {
            List<GameObject> temp = new List<GameObject>();
            if (child.tag == "Rows") 
            {
                foreach (Transform rowItem in child)
                {
                    temp.Add(rowItem.gameObject);
                    GameObject griditem = Instantiate(gridItemPrefab, rowItem.transform.position, rowItem.transform.rotation);
                    griditem.transform.SetParent(rowItem);
                }
                grid.Add(temp);
            } 
            else if (child.tag == "Row Labels") 
            {
                foreach (Transform label in child)
                {
                    rowLabels.Add(label.gameObject);
                }
            }
            else if (child.tag == "Col Labels")
            {
                foreach (Transform label in child)
                {
                    colLabels.Add(label.gameObject);
                }
            } else {
                Debug.Log("Extra Child in Grid" + child.name);
            }
            count++;
        }
        if(count == 0) 
        {
            Debug.Log("Grid rows are not tagged to Rows");
        }

        isloading = false;
    }

}

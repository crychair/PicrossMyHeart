﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareState : MonoBehaviour {

    public GameObject Filled, Crossed;
    public GameObject Selected;

    public enum State {
        Empty,
        Filled,
        Crossed,
    }

    public bool selected = false;

    public State state;

    public void NextState()
    {
        if (state == State.Empty) {
            state = State.Filled;
        } 
        else if (state == State.Filled) {
            state = State.Crossed;
        }
        else if (state == State.Crossed) {
            state = State.Empty;
        }
    }

    public void ChangeDisplay()
    {
        if (state == State.Crossed) {
            Crossed.SetActive(true);
            Filled.SetActive(false);
        } else if (state == State.Filled) {
            Crossed.SetActive(false);
            Filled.SetActive(true);
        } else if (state == State.Empty) {
            Crossed.SetActive(false);
            Filled.SetActive(false);
        }
    }

    public void OnClick()
    {
        //if(selected) {
            //Debug.Log("Clicked");
            NextState();
            ChangeDisplay();
        //}
    }

    void Update()
    {
        if (selected == true)
        {
            Selected.SetActive(true);
        }
        else
        {
            Selected.SetActive(false);
        }
    }
}

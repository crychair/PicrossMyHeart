﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GridItemAnimation : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler {

    public GameObject manager;
    public GameObject toggleButton;
    public bool isLeftClick = false, isRightClick = false;
    public bool isDragClick = false;
    Animator animator;

    public string gridItemState = "Empty";

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        //Need this block because no Input tells you when somthing is being held down. Only the frame Down and Up.
        if (Input.GetMouseButtonDown(0))
        {
            isLeftClick = true;
        }
        else if(Input.GetMouseButtonDown(1))
        {
            isRightClick = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isLeftClick = false;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            isRightClick = false;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (isLeftClick)
        {
            //Debug.Log("Left click");
            dragClick(true, toggleButton.GetComponent<Toggle>().isOn);
        }
        else if (isRightClick)
        {
            //Debug.Log("Right click");
            dragClick(false, toggleButton.GetComponent<Toggle>().isOn);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            clicked(true, toggleButton.GetComponent<Toggle>().isOn);
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            clicked(false, toggleButton.GetComponent<Toggle>().isOn);
        }
    }


    public void clicked(bool isleft, bool istoggle)
    {
        string trigger = "";
        //Debug.Log(isleft + " " + istoggle);
        if (isleft && istoggle) //Left click Toggle is on Heart 
        {
            //heart anim
            //Debug.Log("Fill");
            trigger = "Fill";
        }
        else if (isleft && !istoggle) //Left click Toggle is on Cross
        {
            //cross anim
            //Debug.Log("Cross");
            trigger = "Cross";
        }
        else if (!isleft && !istoggle) //right click and toggle is on cross
        {
            //heart anim
            //Debug.Log("Fill");
            trigger = "Fill";
        }
        else if (!isleft && istoggle) //right click and toggle is on heart
        {
            //cross anim
            //Debug.Log("Cross");
            trigger = "Cross";
        }

        animator.SetTrigger(trigger);
        updateState(trigger);
        updateManagerState();
    }

    void updateState(string trigger)
    {
        if(trigger == "Fill")
        {
            if(gridItemState == "Empty" || gridItemState == "Crossed") {
                gridItemState = "Filled";
            }
            else
            {
                gridItemState = "Empty";
            }
        }
        else if(trigger == "Cross")
        {
            if (gridItemState == "Empty" || gridItemState == "Filled")
            {
                gridItemState = "Crossed";
            }
            else
            {
                gridItemState = "Empty";
            }
        }
    }

    void updateManagerState()
    {
        //Debug.Log("Update Helper" + gridItemState);
        manager.GetComponent<GridItemHelper>().gridItemClickState = gridItemState;
    }

    public void dragClick(bool isleft, bool istoggle)
    {
        string trigger = "";
        string managerState = manager.GetComponent<GridItemHelper>().gridItemClickState;
        //Debug.Log("DragClick");
        if (isleft && istoggle) //Left click Toggle is on Heart 
        {
            //heart anim
            //Debug.Log("1");
            //Debug.Log(managerState);
            //Debug.Log(gridItemState);
            if (managerState != gridItemState) {
                trigger = "Fill";
            }
        }
        else if (isleft && !istoggle) //Left click Toggle is on Cross
        {
            //cross anim
            //Debug.Log("Cross");
            if (managerState != gridItemState)
            {
                trigger = "Cross";
            }
        }
        else if (!isleft && !istoggle) //right click and toggle is on cross
        {
            //heart anim
            //Debug.Log("Fill");
            if (managerState != gridItemState)
            {
                trigger = "Fill";
            }
        }
        else if (!isleft && istoggle) //right click and toggle is on heart
        {
            //cross anim
            //Debug.Log("Cross");
            if (managerState != gridItemState)
            {
                trigger = "Cross";
            }
        }
        animator.SetTrigger(trigger);
        updateState(trigger);
    }
}

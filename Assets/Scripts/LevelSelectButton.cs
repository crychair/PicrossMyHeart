﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectButton : MonoBehaviour {
    public enum puzzleSize
    {
        FiveXFive = 0, TenXTen = 1, FifteenXFifteen = 2, TwentyXTwenty = 3,
    }
    public GameObject levelSizeText;
    public puzzleSize puzzleType = puzzleSize.FiveXFive;
    public int levelNumber = 1;
    public GameObject manager;
    public GameObject levelCompetionTime;
    // Use this for initialization
    void OnEnable () {
        //Debug.Log(levelNumber);
        //Debug.Log((int)puzzleType);
        Level level = manager.GetComponent<GameManager>().GetLevel(levelNumber, (int)puzzleType);
        if (manager.GetComponent<GameManager>().HasKey(level.levelName))
        {
            float completionTime = manager.GetComponent<GameManager>().GetLevelTime(level.levelName);
            int tempMin = (int)completionTime / 60;
            int tempSec = (int)completionTime % 60;
            int tempMili = (int)((completionTime * 100) % 100);
            string temp = string.Format("{0:00}:{1:00}.{2:00}", tempMin, tempSec, tempMili);
            levelCompetionTime.GetComponent<Text>().text = temp;
            levelCompetionTime.SetActive(true);
        } else {
            levelCompetionTime.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadLevel() {
        manager.GetComponent<GameManager>().PuzzleTypeSelect((int)puzzleType);
        manager.GetComponent<GameManager>().LevelSelect(levelNumber);
    }
}
